package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageAble = PageRequest.of(page - 1, size);
        return jpaCompanyRepository.findAll(pageAble).getContent();
    }

    public Company findById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public CompanyResponse findCompanyById(Long id){
        Company storedCompany = findById(id);
        return CompanyMapper.toResponse(storedCompany);
    }

    public CompanyResponse update(Long id, CompanyRequest company) {
        Company storedCompany = findById(id);
        storedCompany.setName(company.getName());
        return CompanyMapper.toResponse(jpaCompanyRepository.save(storedCompany));
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        return CompanyMapper.toResponse(jpaCompanyRepository.save(company));
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow();
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);

    }
}
