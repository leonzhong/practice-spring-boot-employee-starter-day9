package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface JPAEmployeeRepository extends JpaRepository<Employee, Long> {
    public List<Employee> findAllByGender(String gender);
    public List<Employee> findAllByCompanyId(Long companyId);
}
