Objective:
- Today we learned about using Flyway for database management, and introduced the concept of DTOs to refactor our code. In the afternoon we had a retro, as well as group sessions, including microservices, containers, and CI/CD.

Reflective: I feel like I learned a lot today.
	 
Interpretive: 
- In the session, our group talked about microservices. I was responsible for microservice examples. I mainly reviewed the project from my internship, and created a conversational scenario, which was also a review of my internship. Other groups talked about containers and CI/CD, which helped expand my knowledge.
- In the retro, we reviewed our cooperative learning. Next we will spend more time on team collaboration. Each person has different strengths, and it's pointless to get stuck on difficulties if we encounter them while learning. So we need to participate more in discussions, and improve the efficiency of solving problems.
	 
Decision:
- I already have a preliminary understanding of microservices, CI/CD, and containers. I will learn related knowledge in my spare time to improve my development skills.